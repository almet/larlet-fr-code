import re

MD_IMAGE_PATTERN = re.compile(
    r"""
    !\[                             # regular markdown image starter
    (?P<alt>.*?)                    # alternative text to the image
    \]\(                            # close alt text and start url
    (?P<url>.*?)                    # url of the image
    (?:\s*\"(?P<title>.*?)?\")?     # optional text to set a title
    \)                              # close of url-part parenthesis
    """,
    re.VERBOSE | re.MULTILINE,
)
