import fnmatch
import os


def each_folder_from(source_dir, exclude=None):
    """Walk across the `source_dir` and return the folder paths."""
    for direntry in os.scandir(source_dir):
        if direntry.is_dir():
            if exclude is not None and direntry.name in exclude:
                continue
            yield direntry


def each_file_from(source_dir, pattern="*", exclude=None):  # (1)
    """Walk across the `source_dir` and return the matching `pattern` file paths."""
    for filename in fnmatch.filter(sorted(os.listdir(source_dir)), pattern):
        if exclude is not None and filename in exclude:
            continue
        yield source_dir / filename
