class ServerError extends Error {}

function request(url, options) {
  return fetch(url, options)
    .then((response) => [response, response.json()])
    .then(([response, data]) => {
      if (response.ok) {
        return data
      } else {
        const e = new ServerError(`${url} ${response.status} ${data}`)
        e.status = response.status
        e.url = url
        e.data = data
        throw e
      }
    })
    .catch((error) => {
      if (error instanceof ServerError) throw error
      const e = new Error(`${error.message} ${url}`)
      e.url = url
      throw e
    })
}

function handleError(error) {
  console.error(error)
  const errorURL = new window.URL(error.url)
  const userMessage = `
    Le domaine ${errorURL.host} semble être inaccessible.
    Nous en avons été informés, veuillez réessayer plus tard.
  `
}
