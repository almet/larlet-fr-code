function copyToClipboard(codeElement, alert) {
  try {
    navigator.clipboard.writeText(codeElement.innerText)
    alert.innerHTML = `<div class="custom-class">Code copied!</div>`
    setTimeout(() => {
      alert.innerHTML = ''
    }, 3000) // (1)
  } catch (ex) {
    alert.innerHTML = ''
  }
}
