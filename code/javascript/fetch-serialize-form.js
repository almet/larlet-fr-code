const formdata = new FormData(form)
fetch('/test/thing', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
  body: new URLSearchParams(formdata).toString(),
})
  .then((result) => {
    // do something
  })
  .catch((err) => {
    // fix something.
  })
