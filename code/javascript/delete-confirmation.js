function deleteConfirmation(event) {
  if (window.confirm('Are you sure you want to delete this?')) {
    return
  } else {
    event.preventDefault()
  }
}
