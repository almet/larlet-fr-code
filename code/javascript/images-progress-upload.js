;(function uploadImageWithProgress(formSelector, imagesSelector) {
  'use strict'

  const form = document.querySelector(formSelector)
  form.addEventListener(
    'submit',
    (event) => {
      event.stopPropagation()
      event.preventDefault()
      const images = document.querySelectorAll(imagesSelector)
      Array.from(images).forEach((image) => {
        const options = {
          method: event.target.method,
          body: image.file,
        }
        console.log(`Uploading "${image.file.name}"`)
        request(event.target.action, options, displayPercentage)
          .then(console.log.bind(console))
          .catch(console.log.bind(console))
      })
    },
    false
  )

  function request(url, options = {}, onProgress) {
    // See https://github.com/github/fetch/issues/89#issuecomment-256610849
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.open(options.method || 'get', url)
      for (let header in options.headers || {})
        xhr.setRequestHeader(header, options.headers[header])
      xhr.onload = (event) => resolve(event.target.responseText)
      xhr.onerror = reject
      if (xhr.upload && onProgress) xhr.upload.onprogress = onProgress
      xhr.send(options.body)
    })
  }

  function displayPercentage(event) {
    // TODO: use a `progress` HTML element.
    if (event.lengthComputable) {
      const percentage = Math.round((event.loaded * 100) / event.total)
      console.log(percentage)
    }
  }
})('form', 'output img')
