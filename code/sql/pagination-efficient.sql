SELECT * FROM contacts          -- The full data that you want to show your users.
    INNER JOIN (                -- The "deferred join."
        SELECT id FROM contacts -- The pagination using a fast index.
            ORDER BY id
            LIMIT 15 OFFSET 150000
    ) AS tmp using(id)
ORDER BY id                     -- Order the single resulting page as well.
