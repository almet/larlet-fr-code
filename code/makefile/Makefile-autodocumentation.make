.DEFAULT_GOAL := help

.PHONY: install
install: ## Install the dependencies. (1)
	python -m pip install -r requirements.txt
	playwright install


.PHONY: serve
serve: ## Launch a local server to serve the `public` folder. (1)
	@python3 -m http.server 8000 --bind 127.0.0.1 --directory public


.PHONY: test
test: ## Launch all tests. (1)
	@pytest

.PHONY: help
help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# (2)
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
	# if the line has a command and a comment start with
	#   two pound signs, add it to the output
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		output.append("\033[36m%-10s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
