# Python

## :fontawesome-solid-people-line: Voisinage

Lorsqu’on parcoure une liste, il est parfois utile d’avoir accès à
l’élément précédent et au suivant, pour faire par exemple des liens
suivant/précédent entre des billets de blog.

Cette implémentation permet de spécifier le premier et le dernier item
de la liste au besoin, ce qui permet de lier vers d’autres contenus
par exemple.

```py title="neighborhood.py" linenums="1" hl_lines="9 10"
--8<-- "code/python/neighborhood.py"
```

1.  :man_raising_hand: `iterable` peut être une liste, un set ou toute structure… itérable !

Tout se passe au moment du `#!python yield`, on garde en mémoire le 
précédent et le courant lors du parcours de l’itérateur et… on n’oublie
pas le dernier !


## :fontawesome-solid-list: Lister des dossiers/fichiers

Il est courant en Python de devoir parcourir des fichiers ou des dossiers.
Ces deux méthodes permettent de récupérer les chemins des dossiers ou
des fichiers à partir d’une racine.

```py title="each-from.py" linenums="1" hl_lines="5 14"
--8<-- "code/python/each-from.py"
```

1.  Le paramètre de `pattern` est bien pratique pour ne récupérer 
    que des `*.md` par exemple.


## :fontawesome-solid-hashtag: Générer un md5

Parce que j’oublie à chaque fois comment faire…

```py title="md5.py" linenums="1"
--8<-- "code/python/md5.py"
```

Il y aurait probablement de meilleurs algorithmes à utiliser selon les cas.


## :material-link: Générer une chaîne de 128 bits

Pour avoir des slugs dans une URL par exemple.

```py title="128bits-string.py" linenums="1"
--8<-- "code/python/128bits-string.py"
```

Pour un usage réel, il faudrait exclure certains caractères qui peuvent
prêter à confusion. Mais qui dicte des URLs ?


## :material-data-matrix: Générer une data URI

Il est possible de transformer des images en chaîne de caractère afin
de les insérer directement dans le HTML.

```py title="data-uri.py" linenums="1"
--8<-- "code/python/data-uri.py"
```


## :material-update: Récupérer la valeur epoch depuis une date ISO

Parce que j’ai été surpris que ce soit aussi compliqué.
Je suis peut-être passé à côté d’un truc plus élégant…

```py title="to-epoch.py" linenums="1"
--8<-- "code/python/to-epoch.py"
```

1.  Désactivation de black :
    on veut faire rentrer le code dans la page manuellement.

!!! bug "TODO"
    Configurer `black` pour avoir des lignes plus courtes par défaut.
