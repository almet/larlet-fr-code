# Colophon

Ce site est construit avec [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/) et
le fichier de configuration est le suivant :

```yaml title="mkdocs.yml" linenums="1"
--8<-- "mkdocs.yml"
```

1.  :man_gesturing_no: désactivation des polices Google


Il utilise les polices [FiraCode](https://github.com/tonsky/FiraCode) et [GolosUI](https://www.paratype.com/fonts/pt/golos-ui) définies ainsi :

```css title="fonts.css" linenums="1" hl_lines="2 10 18"
--8<-- "docs/static/stylesheets/fonts.css"
```


Les terminaux qui s’animent sont faits à base de [Termynal](https://github.com/ines/termynal),
en s’inspirant de la documentation de [FastAPI](https://fastapi.tiangolo.com/), 
[l’initialisation](https://github.com/tiangolo/fastapi/blob/master/docs/en/docs/js/custom.js) est faite ainsi :

```js title="termynal-setup.js" linenums="1"
--8<-- "docs/static/javascript/termynal-setup.js"
```

Une adaptation a été [tirée de FastAPI](https://github.com/tiangolo/fastapi/blob/master/docs/en/docs/js/termynal.js) pour avoir des boutons
de passage rapide et recommencement :

```js title="termynal.js" linenums="1"
--8<-- "docs/static/javascript/termynal.js"
```

!!! bug "TODO"
    Appliquer mes propres conseils et ne pas lancer l’animation en fonction des paramètres utilisateur·ice !
