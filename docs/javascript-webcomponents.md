# Web components et JavaScript


## :material-timer-sand: Chargement des Web Components

Les Web Components étant chargés en JavaScript, il y a un moment où il
peut y avoir un flash lors du chargement.
Cette astuce [proposée par Cory LaViska](https://www.abeautifulsite.net/posts/flash-of-undefined-custom-elements/) permet de donner une
impression de chargement synchrone à la page.

Plutôt que de le faire en CSS :

```css linenums="1"
:not(:defined) {
  visibility: hidden;
}
```

On va le faire en JavaScript (combiné à du CSS) :

```html title="web-components-loading.html" linenums="1"
--8<-- "code/html/web-components-loading.html"
```

On attend que chacun des composants soit chargé pour afficher le
`#!html <body>` avec une transition sur l’opacité. Selon la portée de
vos composants, il n’est pas forcément nécessaire d’appliquer cela à
la page entière mais ça donne des idées.


## :material-walk: Aller plus loin/différemment

* [Add Responsive-Friendly Enhancements to &lt;details> with &lt;details-utils>](https://www.zachleat.com/web/details-utils/)
* [Web Components as Progressive Enhancement](https://cloudfour.com/thinks/web-components-as-progressive-enhancement/)
* [Building tabs in Web Components](https://darn.es/building-tabs-in-web-components/)
* [Nude UI: A collection of accessible, customizable, ultra-light web components](https://nudeui.com/)
* [Shoelace: A forward-thinking library of web components.](https://shoelace.style/)
* [Lego](https://lego.js.org/)
