# Upload d’images et JavaScript

!!! info "Un tutoriel ?"
    Cette page est davantage la description pas à pas d’une page qui n’a
    jamais été mise en production. Je souhaitais explorer le 
    téléversement d’une image avec son affichage préalable et la 
    possibilité de l’ajouter par glisser-déposer. Il y aurait beaucoup
    de choses à peaufiner avant d’avoir une version utilisable
    (ne serait-ce qu’au niveau de l’accessibilité) mais ça peut vous 
    inspirer.


## :sparkles: L’exemple complet

On commence avec l’exemple complet car c’est toujours chouette de jouer avec en direct :

[Ouvrir dans un nouvel onglet](/examples/images-upload.html){ .md-button target="_blank" }

Vous pouvez essayer d’ajouter des images, on de les déposer dans la zone dédiée.
Rien n’est envoyé sur le serveur.
On a une prévisualisation des images sélectionnées et la capacité de le
faire au clavier (ce qui est toujours un bon exercice d’accessibilité).


## :fontawesome-brands-html5: On commence par le HTML et la CSS

On commence par une base qui permet de soumettre des images sans aucun
JavaScript pour faire de l’amélioration progressive :


```html title="images-form.html" linenums="1" hl_lines="12-14"
--8<-- "code/html/images-form.html"
```

1. On n’oublie pas d’indiquer que l’on souhaite envoyer des fichiers.
2. On accepte toutes formes d’images et on ajoute un attribut pour spécifier la taille maximale (qui sera appliquée en JS).
3. On style le `#!html label` comme un bouton car on va utiliser une astuce.
4. On prépare un endroit où afficher nos erreurs.
5. On utilise [l’élément `#!html output`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/output) pour la prévisualisation de l’image, qui permet d’afficher le résultat d’une action utilisateur·ice.
6. On utilise un élément `#!html canvas` pour délimiter la zone où l’on va pouvoir glisser-déposer les images.

Au sujet du label stylé comme un bouton, il s’agit d’une astuce [issue de cet article](https://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/) qui consiste à masquer l’`#!html input` en lui-même car le `#!html label` a les mêmes propriétés au clic et que le bouton natif n’est pas si évident à styler. Surtout lorsqu’on veut garder la cohérence avec la zone de dépôt.

Je n’ajoute pas la CSS complète ici car c’est vite verbeux
(elle est dans l’exemple), je veux juste montrer ce qui est nécessaire
pour que le `#!html label` nous serve à sélectionner des images :

```css title="images-form.css" linenums="1"
--8<-- "code/css/images-form.css"
```


## :fontawesome-solid-arrow-pointer: Pouvoir cliquer sur la zone de dépôt

```js title="images-selector-dropzone.js" linenums="1" hl_lines="6"
--8<-- "code/javascript/images-selector-dropzone.js"
```

C’est assez direct, on simule le clic sur l’`#!html input`
lorsqu’on clic sur le `#!html canvas` défini plus haut.


## :material-button-pointer: Avoir un retour au glisser

Une autre chose qui est pertinente, c’est de pouvoir avoir un retour
visuel lorsqu’on passe l’image sur la zone où l’on peut la déposer.

Ici on ajoute une classe `active` à notre élément lorsqu’on passe dessus,
la principale difficulté étant de devoir écouter plusieurs évènements
pour la même action.

```js title="images-highlight-dropzone.js" linenums="1"
--8<-- "code/javascript/images-highlight-dropzone.js"
```

1. Ajout de la classe `active` au `canvas`.
2. Retrait de la classe `active` au `canvas`.
3. Fragment qui permet d’écouter une même fonction sur plusieurs évènements.


## :octicons-image-16: Prévisualiser les images

Ici c’est un gros morceau. On écoute un `change` sur l’`#!html input` 
ou un `drop` sur notre dropzone (élément `#!html canvas`).
À partir de là, on va vérifier si la taille limite est dépassée 
et afficher un message d’erreur si c’est le cas ou afficher l’image
téléversée si elle est inférieure à la taille autorisée.

```js title="images-preview-upload.js" linenums="1"
--8<-- "code/javascript/images-preview-upload.js"
```


## :material-progress-upload: Visualiser la progression

Pour finir, on voudrait pouvoir donner un retour à l’utilisateur·ice
au moment du téléversement avec un pourcentage d’avancement.

```js title="images-progress-upload.js" linenums="1"
--8<-- "code/javascript/images-progress-upload.js"
```

!!! bug "Visualiser la progression"
    Ça serait pas mal d’avoir un endroit où faire le téléversement
    pour aller jusqu’au bout de la démo. Peut-être via [httpbin](http://httpbin.org/) ?


## :material-walk: Aller plus loin/différemment

* [Using files from web applications](https://developer.mozilla.org/en-US/docs/Web/API/File_API/Using_files_from_web_applications)
* [Styling & Customizing File Inputs the Smart Way](https://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/)
* [File Field - HTTP and Forms](https://eloquentjavascript.net/18_http.html#h_tK84z183/8)
