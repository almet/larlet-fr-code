# Makefile

## :fontawesome-solid-wand-magic-sparkles: Auto-documentation

Il y a de très nombreuses façon de le faire (voir plus bas).
J’apprécie la version Python qui, même si elle est plus verbeuse,
a l’avantage d’être compréhensible avec mes connaissances actuelles.

```make title="Makefile" linenums="1" hl_lines="23-40"
--8<-- "code/makefile/Makefile-autodocumentation.make"
```

1.  :material-file-document-edit: On documente les commandes directement 
    dans le Makefile
2.  :material-language-python: Le script Python commence ici et il [vient de là-bas](https://daniel.feldroy.com/posts/autodocumenting-makefiles).

Une fois les commentaires ajoutés au `Makefile`, ils deviennent
accessibles sous forme d’aide lorsqu’on lance la commande `make` :


<div class="termy">

```console
$ make

install    Install the dependencies.
serve      Launch a local server to serve the `public` folder.
test       Launch all tests.
```

</div>

## :material-walk: Aller plus loin/différemment

* [Autodocumenting Makefiles](https://daniel.feldroy.com/posts/autodocumenting-makefiles) (la source du script Python ci-dessus)
* [Self-Documented Makefile](https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html)
* [make help - Well documented Makefiles](https://www.thapaliya.com/en/writings/well-documented-makefiles/)
* [Self-Documenting Makefiles](https://www.client9.com/self-documenting-makefiles/)
* [Add a help target to a Makefile that will allow all targets to be self documenting](https://gist.github.com/prwhite/8168133)
* [Why I Prefer Makefiles Over package.json Scripts](https://spin.atomicobject.com/2021/03/22/makefiles-vs-package-json-scripts/)
* [Tips for your Makefile with Python](https://blog.mathieu-leplatre.info/tips-for-your-makefile-with-python.html)
* [Learn Makefiles](https://makefiletutorial.com/)
