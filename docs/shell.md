# Shell

J’utilise actuellement `zsh` qui est présent de base dans macOS.
Je n’ajoute que très peu de scripts pour rester aussi proche de
l’environnement de personnes débutantes que je pourrais accompagner.

Vous pouvez ajouter ces lignes dans votre fichier `~/.zshrc` et
ça ne devrait pas être très éloigné de la syntaxe des autres shells.

## :material-bike-fast: Alias

Je me contente de ceux-ci présentement, c’est principalement 
pour gérer mes environnements Python plus facilement :

```shell title="aliases.sh" linenums="1"
--8<-- "code/shell/aliases.sh"
```

Je sais que les CoolKids® utilisent Poetry ou Pyenv ou que sais-je
encore mais je crois que je préfère encore savoir ce qu’il se passe
sur ma machine.


## :material-account-reactivate: Auto dé·activation des virtualenvs

Script permettant d’activer (ou de désactiver) automagiquement un
`virtualenv` Python lorsqu’on fait un `#!shell cd` dans un dossier qui en
possède un.

```shell title="activation-venv.sh" linenums="1"
--8<-- "code/shell/activation-venv.sh"
```

1.  Au passage, on ajoute `node_modules/.bin` au `$PATH`.
