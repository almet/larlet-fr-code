# HTML

## :octicons-repo-template-24: Un gabarit minimaliste

Parce qu’il faut bien copier-coller une source pour démarrer :fontawesome-solid-face-smile-beam:.


```html title="template.js" linenums="1"
--8<-- "code/html/template.html"
```

1.  Pour en faire un document valide HTML5.
2.  Pour les lecteurs d’écran, le référencement, les extensions, etc.
3.  Doit être dans les premiers 1024 bytes, plutôt à mettre avant le 
    `<title>`, [référence à ce sujet](https://www.w3.org/TR/2012/CR-html5-20121217/document-metadata.html#charset).
4.  Pourquoi pas de balise `meta` avec une valeur `X-UA-Compatible` ?
    Voir [cette réponse sur StackOverflow](https://stackoverflow.com/a/6771584).
5.  Beaucoup de monde dans la balise `meta` relative au `viewport`,
    et [nous sommes responsables de cela…](https://codepen.io/tigt/post/meta-viewport-for-2015)
6.  Avec cette valeur, le site lié ne saura pas de quelle page provient
    le lien qui a été cliqué mais aura uniquement l’information du
    domaine. Cela me semble être un bon compromis pour préserver
    l’intimité des personnes.
7.  Gif transparent le plus petit qui soit, évite une requête inutile
    vers le serveur (et une erreur dans la console).
