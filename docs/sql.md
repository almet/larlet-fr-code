# SQL

Je regrette de ne pas avoir sauvegardé mes expériences dans ce domaine
au cours du temps. C’est d’autant plus frustrant que je me sens très
incompétent dans le domaine…

!!! hint "Quelques astuces"
    1. Utiliser un timestamp à la place d’un booléen, ce qui permet
       d’avoir la date d’établissement du booléen envisagé.
    2. Toujours mettre une condition `LIMIT`, quelle que soit la requête,
       ça évite de se retrouver avec des problèmes de mémoire plus tard…


## :material-skip-next: Pagination efficace

Un fragment [issu de cet article](https://aaronfrancis.com/2022/efficient-pagination-using-deferred-joins). Cette méthode permet de ne pas
faire des requêtes de plus en plus lourdes au cours de la pagination
lorsque l’`#!sql offset` augmente.

```sql title="pagination-efficient.sql" linenums="1"
--8<-- "code/sql/pagination-efficient.sql"
```


## :material-counter: Compter plusieurs fois avec Django

Une astuce grâce à [ce commentaire dans les tickets Django](https://code.djangoproject.com/ticket/10060#comment:67).

```py title="django-annotate-sum.py" linenums="1"
--8<-- "code/python/django-annotate-sum.py"
```

Qui peut ensuite être utilisé ainsi :

```py linenums="1"
my_instances = (
    MyModel.objects
    .annotate_sum('points', 'points_earned')
    .annotate_sum('points', 'points_spent')
)
```


## :material-walk: Aller plus loin/différemment

* [Cours et tutoriels sur le langage SQL](https://sql.sh/)
* [Expressive analytics in Python at any scale.](https://ibis-project.org/)
