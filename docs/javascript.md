# JavaScript

## :material-clock-fast: Raccourci pour sélectionner une liste de nœuds

La méthode standard qui consiste à utiliser `#!js querySelectorAll()`
est relativement verbeuse et ne permet pas de faire facilement un
`#!js forEach()` dessus. Cette version simplifiée est un raccourci utile
lorsqu’on doit le faire souvent :

```js title="qsa.js" linenums="1"
--8<-- "code/javascript/qsa.js"
```

1.  On en profite pour retourner un `#!js Array` !


## :fontawesome-solid-anchor-circle-check: Récupérer la chaîne de l’ancre d’une URL

Je passe toujours trop de temps à retrouver comment faire.

```js title="anchor.js" linenums="1"
--8<-- "code/javascript/anchor.js"
```


## :material-emoticon-sick-outline: Prendre en compte les préférences utilisateur·ice pour lancer une animation

Très important car des personnes sont mal à l’aise avec certaines 
animations (j’en fait partie, parfois).

```js title="motion.js" linenums="1"
--8<-- "code/javascript/motion.js"
```


## :material-calendar-plus: Raccourci pour créer/attacher un évènement

Un bout de code qui vient de Chris Ferdinandi
(voir [1](https://gomakethings.com/custom-events-with-vanilla-js/), [2](https://gomakethings.com/a-vanilla-js-custom-event-helper-function/) et [3](https://gomakethings.com/custom-event-naming-conventions/)) :

```js title="emit.js" linenums="1"
--8<-- "code/javascript/emit.js"
```

Il peut s’utiliser ainsi 
`#!js emit('calculator:add', {total: 20}, calendarElement)`
et s’écouter de manière classique.


## :material-bandage: Polyfills à la demande

Il est possible d’importer des `polyfills` externes directement 
depuis une balise `#!html <script>`, par exemple pour 
`Promise` ou `fetch` :

```js title="polyfills.html" linenums="1"
--8<-- "code/html/polyfills.html"
```

En pratique, c’est quand même mieux de ne pas faire des requêtes
externes et d’avoir les fichiers en local.


## :material-clipboard-edit: Copier dans le presse-papier

Lorsqu’il y a un champ avec un code ou une URL à copier-coller, ça peut
être intéressant de proposer à l’utilisateur·ice de cliquer pour mettre
l’information dans son presse-papier et le coller ailleurs.

```js title="copy-clipboard.js" linenums="1"
--8<-- "code/javascript/copy-clipboard.js"
```

1. Vous pouvez adapter cette valeur qui correspond au temps d’affichage
   de l’information (en millisecondes).


## :material-delete-alert: Confirmation de suppression

La façon la plus simple d’ouvrir une popup de confirmation.

```js title="delete-confirmation.js" linenums="1"
--8<-- "code/javascript/delete-confirmation.js"
```

!!! danger "Fermeture automatique"
    Notez qu’à force de recevoir des notifications, les utilisateur·ices
    sont promptes à fermer une fenêtre, aussi rouge soit-elle.
    Il peut-être pertinent de demander une confirmation qui demande de
    saisir quelque chose comme le fait Github lorsqu’on supprime un
    dépôt (il faut taper le nom complet du dépôt concerné).


## :octicons-link-16: Générer un slug

Pour transformer un titre en français en une portion d’URL par exemple.
Adapté depuis [cet article](https://mhagemann.medium.com/the-ultimate-way-to-slugify-a-url-string-in-javascript-b8e4a0d849e1).

```js title="slugify.js" linenums="1"
--8<-- "code/javascript/slugify.js"
```


## :material-puzzle: Un système de plugins

Ce fragment est issu de [ce qu’à fait Simon Willison](https://simonwillison.net/2021/Jan/3/weeknotes/)
pour Datasette, aidé [par Matthew Somerville](https://github.com/simonw/datasette/issues/983#issuecomment-752888552).

```js title="plugins.js" linenums="1"
--8<-- "code/javascript/plugins.js"
```

Il s’utilise ensuite ainsi :

```js linenums="1"
datasette.plugins.register('numbers', ({a, b}) => a + b)
datasette.plugins.register('numbers', o => o.a * o.b)
datasette.plugins.call('numbers', {a: 4, b: 6})
```


## :material-walk: Pour aller plus loin/différemment

* [1LOC](https://1loc.dev/)
* [HTML DOM](https://htmldom.dev/)
* [THIS vs. THAT](https://thisthat.dev/)
* [The Vanilla JS Toolkit](https://vanillajstoolkit.com/)
