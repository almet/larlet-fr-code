# Formulaires et JavaScript

!!! question "C’est quoi ce bazard ?"
    Vous remarquerez que les solutions de double soumission suivantes 
    sont toutes assez différentes.
    Je n’essaye pas d’ajouter de cohérence car elles ont toutes leurs 
    subtilités qui m’ont permis d’apprendre des propriétés de JavaScript.

## :material-send-lock: Éviter la multiple soumission

Une solution que je pensais [venir de là](https://ma.ttias.be/double-clicking-on-the-web/) mais en fait non.
Ou alors le billet de blog a changé depuis.

```js title="forms-double-submit.js" linenums="1"
--8<-- "code/javascript/forms-double-submit.js"
```

1. Possibilité d’adapter la valeur du `#!js Timeout`.


## :material-progress-upload: En ajoutant une barre de progression

Une proposition qui [vient de Jeremy Keith](https://gist.github.com/adactio/9315750).

```js title="forms-double-submit-progress.js" linenums="1"
--8<-- "code/javascript/forms-double-submit-progress.js"
```


## :material-progress-check: Avec déclaration explicite

Une proposition qui [vient de Andrea Giammarchi](https://gist.github.com/WebReflection/15fc0a2bbdd5afc7a669).

```js title="forms-double-submit-explicit.js" linenums="1"
--8<-- "code/javascript/forms-double-submit-explicit.js"
```


## :material-image: Prévisualiser les images téléversées

Un bout de code qui [vient de Jeremy Keith](https://adactio.com/journal/19080) :

```js title="preview-image-upload.js" linenums="1"
--8<-- "code/javascript/preview-image-upload.js"
```

1.  Pour en savoir plus sur [la notion de moutarde coupée](https://www.filamentgroup.com/lab/enhancing-optimistically.html) 
    (et [une version plus moderne](https://snugug.com/musings/modern-cutting-the-mustard/)).

Il devrait être effectif que tous les inputs de ce type :
`#!html <input type="file" accept="image/*">`.

!!! hint "Aller plus loin avec les images ?"
    Un exemple complet est documenté [sur une page dédiée](javascript-images.md).


## :material-form-textbox: Sauvegarder le contenu des textareas

Encore(!) un bout de code qui [vient de Jeremy Keith](https://adactio.com/journal/17516) :

```js title="forms-save-textarea.js" linenums="1"
--8<-- "code/javascript/forms-save-textarea.js"
```

1.  Pour en savoir plus sur [la notion de moutarde coupée](https://www.filamentgroup.com/lab/enhancing-optimistically.html) 
    (et [une version plus moderne](https://snugug.com/musings/modern-cutting-the-mustard/)).

!!! warning "Nécessite une adaptation"
    Comme le fait remarquer Jeremy en commentaire, il faut adapter le
    sélecteur car vous avez probablement plus d’un textarea dans la page.
    Il serait possible de le rendre générique mais il y a peut-être des
    cas où vous ne voulez pas que ça sauvegarde, je préfère le laisser
    en explicitement activable.
