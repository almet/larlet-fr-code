# CSS

## :fontawesome-solid-scroll: Scroller en douceur

```css title="smooth.css" linenums="1"
--8<-- "code/css/smooth.css"
```


## :material-emoticon-sick-outline: Prendre en compte les préférences utilisateur·ice pour lancer une animation

```css title="motion.css" linenums="1"
--8<-- "code/css/motion.css"
```

Une autre option est de le faire directement en ne chargeant la CSS
dédiée aux animations que si la préférence utilisateur·ice
la rend acceptable :

```css title="motion-link.html" linenums="1"
--8<-- "code/html/motion-link.html"
```


## :octicons-video-16: Avoir un bon ratio pour les vidéos

Une façon [documentée par ici](https://timothymiller.dev/posts/2022/stop-using-the-padding-hack/) pour afficher des incrustations
vidéos dans un format convenable :

```css title="video-ratio.css" linenums="1"
--8<-- "code/css/video-ratio.css"
```


## :material-radiobox-blank: Un reset minimaliste

Un reset très bien [documentée par ici](https://www.joshwcomeau.com/css/custom-css-reset/) qui est relativement
court :

```css title="reset.css" linenums="1"
--8<-- "code/css/reset.css"
```

J’ajoute souvent aussi ces quelques règles que j’estime être importantes
pour avoir des styles par défaut cohérents :

```css title="reset-extras.css" linenums="1"
--8<-- "code/css/reset-extras.css"
```

Il y a d’autres projets comme [minireset.css](https://github.com/jgthms/minireset.css) qui existent.


## :material-domino-mask: Masquer du contenu

Il y a plusieurs façons de le faire mais je trouve que la meilleure
explication vient de Kitty Giraudel à travers [deux](https://kittygiraudel.com/2020/12/03/a11y-advent-hiding-content/) [articles](https://kittygiraudel.com/2020/12/06/a11y-advent-skip-to-content/)
de 2020.

```css title="screen-readers-only.css" linenums="1"
--8<-- "code/css/screen-readers-only.css"
```

C’est ensuite utilisable ainsi, par exemple pour un lien d’accès direct
au contenu où l’on a besoin de conserver le focus :

```html
<a href="#main" class="sr-only sr-only--focusable">Skip to content</a>
```


## :material-oil: Changement de graisse

Lorsqu’on clic sur un élément pour le rendre **gras** (ou l’inverse),
ça modifie l’espace réservé par le navigateur pour ce(s) terme(s).
Parfois, cela peut engendrer des effets disgracieux (hover sur un lien,
item actif d’un onglet, etc).

Une solution [trouvée par ici](https://darn.es/building-tabs-in-web-components/#adjusting-for-tab-weight-changes) est de charger le contenu en **gras**
dans un `#!css ::before`

```css title="reserve-bold-space.css" linenums="1"
--8<-- "code/css/reserve-bold-space.css"
```

On peut ensuite l’appeler ainsi dans le HTML :

```html linenums="1"
<div class="reserve-bold-space" data-text="The tab label">
    The tab label
</div>
```

Notez bien qu’il faut que le contenu du `#! data-text` soit le même que
le contenu affiché !


## :fontawesome-solid-medal: Augmenter la spécificité d’une classe

Si vous n’avez accès qu’à une classe et qu’il vous faut la spécificité
d’un id par exemple, il est possible d’utiliser `#!css :not()` avec un
id ce qui va artificiellement passer ce sélecteur à une spécificité d’id
même si celui-ci n’existe pas ! Merci [Robert Koritnik](https://erraticdev.blogspot.com/2018/11/css-class-selector-with-id-specificity.html).

```css title="bump-specificity.css" linenums="1"
--8<-- "code/css/bump-specificity.css"
```

Pour tester les spécificités en CSS : [Specificity Calculator](https://specificity.keegan.st/)


## :scientist: Cas d’usages de `#!css is()` et `#!css has()`

!!! danger "Support navigateur"
    Attention, si `#!css is()` n’est [pas trop mal supporté](https://caniuse.com/css-matches-pseudo),
    `#!css has()` n’est [pas implémenté par de nombreux navigateurs](https://caniuse.com/css-has)
    à ce jour (22 août 2022). Je consigne les cas d’usage ici pour
    l’avenir mais ça n’est pas envisageable en production pour l’instant.

Plein d’exemples qui viennent [du blog de webkit](https://webkit.org/blog/13096/css-has-pseudo-class/).

Lorsque tu veux que les éléments qui suivent un titre soient proches
de ce titre :

```css title="has-titles.css" linenums="1"
--8<-- "code/css/has-titles.css"
```

Lorsque tu veux que ton formulaire t’indique un peu mieux où est
l’erreur :

```css title="has-errors.css" linenums="1"
--8<-- "code/css/has-errors.css"
```

Ici, j’applique un style au `#!css label` mais ça pourrait être
plus large bien entendu, ça ouvre de très nombreuses possibilités !

Lorsque tu veux changer des variables CSS en fonction d’un
choix utilisateur·ice (par exemple un sélecteur de thème) :

```css title="has-choice.css" linenums="1"
--8<-- "code/css/has-choice.css"
```

Ça ne fait qu’effleurer les possibilités mais ça promet :material-robot-happy-outline:.

Un [article par Bramus Van Damme](https://www.bram.us/2021/12/21/the-css-has-selector-is-way-more-than-a-parent-selector/) à ce sujet ainsi qu’un
[article sur le blog de Chrome](https://developer.chrome.com/blog/has-m105/) qui donnent d’autres exemples.
